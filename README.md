# This repository contains the code to reproduce all experiments (figures and simulations) of the paper "Learning Algorithms for Markovian Bandits: Is Posterior Sampling more Scalable than Optimism?"

## Reproducing the Counter-Example of the proof of Theorem 3

The file `Counter-example of paper.ipynb` contains a description of the example used in Appendix D as well as the code to compute extended value iteration for this example.

## Plotting the Figures

The file `figures.ipynb` contains all necessary scripts to plot the figures of the paper.

If `Nix` is installed, you can launch directly
```console
$ nix-shell shell.nix --run 'jupyter notebook'
```

This file used the data from simulations that we have launched. The results of the simulation can also be reproduced, by following the instruction below. Note that regenerating all data might take **around a week** in a single-core machine.

### Install `Nix`

To reproduce our results, we require to have `Nix` installed. `Nix` is the __only requirement__ for all experiments below. You can install `Nix` by following the instructions on https://nixos.org/download.html

Note: for macOS >= 10.15, the installation of `Nix` can be complicated due to `SIP` of macOS. We do not have an easy fix for this but note that `Nix` is easily installed in a Linux virtual machine.

### Launching the simulation
Be sure that `launchWithNix.sh` is executable and just launch:
```console
./launchWithNix.sh
```
This will successively launch the simulation for the three experiments.

### Experiment time

Using a single-core machine, we expect that the experiments take around a week.

## Script Explanation

The bash script `launchWithNix.sh` starts by verifying whether `Nix` is installed. If no, the script prints some messages and exits. Otherwise, the script launches `Nix` command

```console
nix-shell --pure shell.nix --run "python setup.py build_ext --build-lib './utils' && mpiexec -n $NB_PROCS python mpi_regret.py $DISCOUNT $EPISODES $NB_SIMS $SCENE"
```

for Scenario 1 and 2, and

```console
nix-shell --pure shell.nix --run "python setup.py build_ext --build-lib './utils' && mpiexec -n $NB_PROCS python mpi_regret.py $DISCOUNT $EPISODES $NB_SIMS_PER_MDP $SCENE $NB_MDPS_PER_PROC"
```

for Scenario 3.

So, `Nix` will create an environment respecting the package requirement in `shell.nix` file. This takes several minutes. Then, `Nix` launches a shell in created environment and executes the command `python setup.py build_ext --build-lib "./utils" && mpiexec -n $NB_PROCS python mpi_regret.py $DISCOUNT $EPISODES $NB_SIMS_PER_MDP $SCENE $NB_MDPS_PER_PROC` inside the shell.

### Python Packages installed by Nix in the created environment

- numpy
- matplotlib
- seaborn
- cython
- mpi4py
- h5py
- setuptools
- tqdm
- jupyter
- ipython

These packages are specified in `shell.nix` so that `Nix` can build an environment with these packages installed. Other 2 packages specified in `shell.nix` are `openmpi` and `openssh`

### Python Script

The main script is `mpi_regret.py` which runs all algorithms (MB-PSRL, MB-UCRL2 and MB-UCBVI) and tracks their regret and computation time needed. First, `Nix` builds the executables (or modules) which is done by command

```console
python setup.py bulid_ext --build-lib './utils'
```

This command compiles all the files `*.pyx` and puts the generated modules in `utils` folder.

## Models

We test the algorithms on 2 models:

1. Random Walk

A Random Walk chain can be obtained by applying the optimal policy on River Swim MDP (which is a hard-to-learn example in MDP literature). The figure below depicts this chain.

![A bandit which is a Random Walk chain](RandomWalk.png)

There are 4 states and only the first and the last state are rewarding by $`r_L`$ and $`r_R`$ in expectation respectively.
We have

- $`p_R`$ is the probability of going right
- $`p_L`$ is the probability of going left
- $`p_{RL}`$ is the probability of going left when currently at the rightmost state

In the code, we can increase or decrease the number of states by modifying the parameter `nb_states` in `mpi_regret.py`. Each Random Walk chain has different values of $`p_R, p_L, p_{RL}, r_L`$ and $`r_R`$.

The random reward has Bernoulli distribution. We implement 2 posterior distributions: Beta and Gaussian-Gamma which are the conjugate posterior of Uniform and Gaussian-Gamma distributions respectively.
In our code, by default, the reward distribution of the environment is Bernoulli.

2. Task Scheduling

We also implement the Task Scheduling scenario described on page 19 in https://moduff.github.io/Duff_1995_q_learning_bandits.pdf.
The Markov chain is given in the figure below.
Each task has 10 states and 1 absorbing state (finished state).
The hazard rate $`\rho_i`$ is increasing in $`i\in\{1, 2,\dots, 10\}`$.

![A Task Scheduling with 11 states](Task_Scheduling.png).

When transitioning from state $`i`$ to $`\star`$, the reward is $`1`$.
Otherwise, the reward is always $`0`$.


__Note__: the reward in Task Scheduling is deterministic.

## Gittins Index computation

The method for Gittins Index computation is written in `experiment.pyx`. We use the State Elimination algorithm introduced by Isaac Sonin which is described in section 24.3.2 of http://www.ece.mcgill.ca/~amahaj1/projects/bandits/book/2013-bandit-computations.pdf.
