# -*- coding: utf-8 -*-

""" Convert from Restful Markovian bandits to Markov Decision Process (MDP)

- xyz_to_state: convert from vector of states to state of MDP
- state_to_xyz: reversion of xyz_to_state
- make_global_MDP: return the transition matrix and reward function of MDP given the transition matrices and reward vectors of all Markovian bandits

"""

from __future__ import division, print_function # Python 2 compatibility



cimport numpy as cnp
import numpy as np

DTYPE_I = np.int
DTYPE_F = np.float64

ctypedef cnp.int_t DTYPE_Ic
ctypedef cnp.float64_t DTYPE_Fc

cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)
def xyz_to_state(cnp.ndarray[DTYPE_Ic, ndim=1] arr, int N_bandits, int nb_states):
    """
    Convert Markovian bandit state to global MDP state

    Parameters
    ----------
    arr : ndarray
        State of Markovian bandits
    N_bandits : int
        Number of the bandits
    nb_states : int
        Number of states in each bandit

    Returns
    -------
    out : corresponding global MDP state
    """
    if arr.shape[0] != N_bandits or (arr >= nb_states).any():
        raise ValueError("False arguments")
    assert arr.dtype == DTYPE_I
    cdef int s = 0
    cdef Py_ssize_t i
    for i in range(N_bandits):
        s += arr[i]*pow(nb_states, N_bandits-i-1)
    #return arr[2] + arr[1]*nb_states + arr[0]*nb_states*nb_states
    return s

def state_to_xyz(int s, int N_bandits, int nb_states):
    """
    Convert global MDP state to Markovian bandit state
    """
    cdef cnp.ndarray[DTYPE_Ic, ndim=1] arr = np.zeros(N_bandits, dtype=DTYPE_I)
    cdef Py_ssize_t i
    for i in reversed(range(N_bandits)):
        arr[N_bandits-i-1] = s // pow(nb_states, i)
        s -= arr[N_bandits-i-1] * pow(nb_states, i)
    return arr

def make_global_MDP(list Ps, list Rs):
    """
    Create a global MDP corresponding to the Markovian Bandit problem

    Parameters
    ----------
    Ps : list
        list of transition matrices of all bandits
    Rs : list
        list a list of reward vectors of all bandits

    Returns
    -------
    transitions : ndarray
        the transition matrix of the global MDP
    rewards : ndarray
        the reward matrix for the global MDP
    """
    cdef Py_ssize_t nb_states = Ps[0].shape[0]
    cdef Py_ssize_t N_bandits = len(Ps)
    cdef int number_of_MDP_states = int(pow(nb_states, N_bandits)) # number of states in global MDP
    cdef cnp.ndarray[DTYPE_Fc, ndim=3] transitions = np.zeros((number_of_MDP_states, N_bandits, number_of_MDP_states), dtype=DTYPE_F) # transition matrix
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] rewards = np.zeros((number_of_MDP_states, N_bandits), dtype=DTYPE_F) # reward vector
    cdef Py_ssize_t a
    cdef int s, sprime
    cdef cnp.ndarray[DTYPE_Ic, ndim=1] xyz, xyz_prime
    for s in range(number_of_MDP_states):
        xyz = state_to_xyz(s, N_bandits, nb_states)
        for a in range(N_bandits):
            rewards[s, a] = Rs[a][xyz[a]]
            xyz_prime = np.copy(xyz)
            for x in range(nb_states):
                xyz_prime[a] = x
                sprime = xyz_to_state(xyz_prime, N_bandits, nb_states)
                transitions[s, a, sprime] = Ps[a][xyz[a], x]
    return transitions, rewards
