# -*- coding: utf-8 -*-

""" Conjugate Beta distribution for Bernoulli prior
https://en.wikipedia.org/wiki/Conjugate_prior

- initial_prior_R: initial the counters for conjugate update
- draw_reward_matrix_for_current_state: draw mean reward for each action from posterior distribution
- draw_reward_matrix: for each state, call draw_reward_matrix_for_current_state
- update_posterior_R: update the counters for conjugate posterior distribution

"""

from __future__ import division, print_function # Python 2 compatibility



cimport numpy as cnp
import numpy as np

DTYPE_I = np.int
DTYPE_F = np.float64

ctypedef cnp.int_t DTYPE_Ic
ctypedef cnp.float64_t DTYPE_Fc

cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)
# For reward
# Bernoulli-Beta posterior sampling
def initial_prior_R(Py_ssize_t number_of_states, Py_ssize_t number_of_actions):
    """
    Return the prior of the reward function
    s_0 = 1
    f_0 = 1

    Returns
    -------
    out : ndarray
        0: success frequency, 1: failure frequency
    """
    return np.ones((2, number_of_states, number_of_actions), dtype=DTYPE_F)

def draw_reward_matrix_for_current_state(cnp.ndarray[DTYPE_Fc, ndim=3] posterior, Py_ssize_t current_state, Py_ssize_t current_action):
    """
    Return the drawn reward for each action when in current_state
    """
    cdef double s = posterior[0, current_state, current_action] # success observation
    cdef double f = posterior[1, current_state, current_action] # failure observation
    return np.random.beta(s, f)

def draw_reward_matrix(cnp.ndarray[DTYPE_Fc, ndim=3] posterior):
    """
    Return the drawn reward matrix for all action of all states

    Returns
    -------
    x : ndarray
        Drawn reward for each state-action pair
    """
    cdef Py_ssize_t number_of_states, number_of_actions
    number_of_states = posterior.shape[1]
    number_of_actions = posterior.shape[2]
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] x = np.zeros((number_of_states, number_of_actions), dtype=DTYPE_F)
    cdef Py_ssize_t s, a
    for s in range(number_of_states):
        for a in range(number_of_actions):
            x[s, a] = draw_reward_matrix_for_current_state(posterior, s, a)
    return x

def update_posterior_R(cnp.ndarray[DTYPE_Fc, ndim=3] posterior, Py_ssize_t current_state, Py_ssize_t action, double reward):
    """
    Update the posterior distribution when we observe that the MDP jumps
    froms 'current_state' to 'next_state' causing by the action 'action' was chosen
    """
    posterior[0, current_state, action] += reward   # success update
    posterior[1, current_state, action] += 1-reward # failure update
