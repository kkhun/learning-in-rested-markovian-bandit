# -*- coding: utf-8 -*-

""" Conjugate Dirichlet distribution for categorical prior distribution
https://en.wikipedia.org/wiki/Conjugate_prior

- initial_prior_T: initial the counters for conjugate update
- draw_transition_matrices_for_current_state: draw transition vector for each action from posterior distribution
- draw_transition_matrices: for each state, call draw_transition_matrices_for_current_state
- update_posterior_T: update the counters for conjugate posterior distribution

"""

from __future__ import division, print_function # Python 2 compatibility



cimport numpy as cnp
import numpy as np

DTYPE_I = np.int
DTYPE_F = np.float64

ctypedef cnp.int_t DTYPE_Ic
ctypedef cnp.float64_t DTYPE_Fc

cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)
# For transition
# Dirichlet posterior sampling
def initial_prior_T(Py_ssize_t number_of_states, Py_ssize_t number_of_actions):
    """
    Return the prior of a MDP of the right size
    - i.e. dirichlet with all values equal to 1
    """
    return np.ones((number_of_states, number_of_actions, number_of_states), dtype=DTYPE_I)

def draw_transition_matrices_for_current_state(cnp.ndarray[DTYPE_Ic, ndim=3] posterior, Py_ssize_t current_state):
    """
    Return the transition vector of each action when in current_state
    """
    cdef Py_ssize_t number_of_states, number_of_actions, a
    number_of_states = posterior.shape[0]
    number_of_actions = posterior.shape[1]
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] draw_matrices = np.zeros((number_of_actions, number_of_states), dtype=DTYPE_F)
    for a in range(number_of_actions):
        draw_matrices[a, :] = np.random.dirichlet(posterior[current_state, a, :])
    return draw_matrices

def draw_transition_matrices(cnp.ndarray[DTYPE_Ic, ndim=3] posterior):
    """
    Draw transitions matrices (for a given prior distribution)

    Returns
    -------
    transition_matrices : ndarray
        The drawn transition matrix
    """
    cdef Py_ssize_t number_of_states, number_of_actions, s
    number_of_states = posterior.shape[0]
    number_of_actions = posterior.shape[1]
    cdef cnp.ndarray[DTYPE_Fc, ndim=3] transition_matrices = np.zeros((number_of_states, number_of_actions, number_of_states), dtype=DTYPE_F)
    for s in range(number_of_states):
        transition_matrices[s, :, :] = draw_transition_matrices_for_current_state(posterior, s)
    return transition_matrices

def update_posterior_T(cnp.ndarray[DTYPE_Ic, ndim=3] posterior, Py_ssize_t current_state, Py_ssize_t action, Py_ssize_t next_state):
    """
    Update the posterior distribution when we observe that the MDP jumps
    froms state 'current_state' to 'next_state' and action "action" was chosen
    """
    posterior[current_state, action, next_state] += 1
