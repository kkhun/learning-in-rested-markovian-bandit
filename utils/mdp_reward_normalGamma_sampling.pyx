# -*- coding: utf-8 -*-

""" Conjugate NormalGamma distribution for normal prior distribution
https://en.wikipedia.org/wiki/Conjugate_prior

- initial_prior_R: initial the counters for conjugate update
- draw_reward_matrix_for_current_state: draw mean reward for each action from posterior distribution
- draw_reward_matrix: for each state, call draw_reward_matrix_for_current_state
- update_posterior_R: update the counters for conjugate posterior distribution

"""

from __future__ import division, print_function # Python 2 compatibility



cimport numpy as cnp
import numpy as np

DTYPE_I = np.int
DTYPE_F = np.float64

ctypedef cnp.int_t DTYPE_Ic
ctypedef cnp.float64_t DTYPE_Fc

cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)
# For reward
# Normal-Gamma posterior sampling
def initial_prior_R(Py_ssize_t number_of_states, Py_ssize_t number_of_actions):
    """
    Return the prior of the reward function

    Returns
    -------
    out : ndarray
        0: average reward, 1: average of square reward, 2: state-actoin pair counter
    """
    return np.zeros((3, number_of_states, number_of_actions), dtype=DTYPE_F)

def draw_reward_matrix_for_current_state(cnp.ndarray[DTYPE_Fc, ndim=3] posterior, Py_ssize_t current_state):
    """
    Return the drawn reward for each action when in current_state
    """
    n_0   = 1
    mu_0  = 0.0
    tau_0 = 1.0

    cdef cnp.ndarray[DTYPE_Fc, ndim=1] n = posterior[2, current_state] # counter
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] mu = (n_0*mu_0 + n * posterior[0, current_state]) / (1 + n) #(n_0*mu_0+n*mu_hat)/(n_0+n)
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] Lambda = n_0 + n #n_0 + n
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] alpha = (n_0 + n) / 2 #n_0/2 + n/2
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] s = posterior[1, current_state] - posterior[0, current_state]**2 #avg_of_squares - (avg)**2
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] beta = n_0/(2*tau_0) + (n*s + (n*(posterior[0, current_state]-mu_0)**2)/(n_0+n))/2 #n_0/(2*tau_0) + (n*s_hat+(n_0*n(mu_hat-mu_0)**2)/(n_0+n))/2
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] tau = np.random.gamma(alpha, beta)
    return np.random.normal(mu, 1./np.sqrt(Lambda*tau))

def draw_reward_matrix(cnp.ndarray[DTYPE_Fc, ndim=3] posterior):
    """
    Return the drawn reward matrix for all action of all states
    NormalGamma(mu, Lambda, alpha, beta)

    Returns
    -------
    x : ndarray
        Drawn reward for each state-action pair
    """
    cdef Py_ssize_t number_of_states, number_of_actions
    number_of_states = posterior.shape[1]
    number_of_actions = posterior.shape[2]
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] x = np.zeros((number_of_states, number_of_actions), dtype=DTYPE_F)
    cdef Py_ssize_t s
    for s in range(number_of_states):
        x[s] = draw_reward_matrix_for_current_state(posterior, s)
    return x

def update_posterior_R(cnp.ndarray[DTYPE_Fc, ndim=3] posterior, Py_ssize_t current_state, Py_ssize_t action, double reward):
    """
    Update the posterior distribution when we observe that the MDP jumps
    froms 'current_state' to 'next_state' and action "action" was chosen
    """
    cdef double n = posterior[2, current_state, action] # counter of (current_state, action) pair
    cdef double mu_hat = (posterior[0, current_state, action] * n + reward) / (n + 1)
    cdef double avg_square = (posterior[1, current_state, action] * n + reward**2) / (n + 1)
    posterior[0, current_state, action] = mu_hat
    posterior[1, current_state, action] = avg_square
    posterior[2, current_state, action] = n + 1
