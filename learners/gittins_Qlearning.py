# -*- coding: utf-8 -*-
""" Q-learning for Bandit problem

Algorithm from http://www.ece.mcgill.ca/~amahaj1/projects/bandits/book/2013-bandit-computations.pdf
"""

from __future__ import division, print_function # Python 2 compatibility



from learners.markovian_bandit_learner import *

def choose_arm(g, tau):
    """
    :param g - vector
    :param tau - scalar
    :return an integer
    """
    if tau < 1e-6:
        return np.argmax(g)
    else:
        g_tilde = np.exp((g - np.max(g))/tau)
        prob = g_tilde / np.sum(g_tilde)
        cumsum_p = np.cumsum(prob)
        return np.where(np.random.rand() < cumsum_p)[0][0]

class Gittins_QLearning(Learner):
    """
    Inheriting UCRL2_Learner class because both classes have the same counters
    """
    def reset(self):
        self.state_counter = np.zeros((self.N_bandits, self.nb_states), dtype=np.float64)
        self.Gittins_Idx = np.zeros((self.N_bandits, self.nb_states), dtype=np.float64)
        self.quality_func = np.zeros((self.N_bandits, self.nb_states, self.nb_states, 2), dtype=np.float64)
        self.temperature = 1
        self.t = 1

    def name(self):
        return "Gittins_QLearning"

    def choose_action(self, xyz):
        """
        Convert Global MDP state into Markvian bandit state and choose bandit according to the index

        Parameters
        ----------
        state : int
            The current state of global MDP

        Returns
        -------
        out : int
            The action for the current state
        """
        Gidx = [(1-self.discount_factor) * self.quality_func[b, xyz[b], xyz[b], 1] for b in range(self.N_bandits)]
        return choose_arm(Gidx, self.temperature)

    def update_policy(self):
        return 0.0 # no time taken to update policy

    def update_history(self, xyz, bandit, reward, next_xyz):
        """
        Returns
        -------
        out : float
            the time taken to update policy
        """
        s = xyz[bandit]                 # current state of bandit
        sprime = next_xyz[bandit]       # next state of bandit

        start_update = timeit.default_timer()
        self.state_counter[bandit, s] += 1
        if self.temperature > 1e-6:
            self.t += 1
            self.temperature = 550/(1+self.t)

        learning_rate = 1e3/(self.state_counter[bandit, s]+1e3)
        for k in range(self.nb_states):
            #continue action
            q_k0 = reward + self.discount_factor*self.quality_func[bandit, k, sprime, 0]
            q_k1 = reward + self.discount_factor*self.quality_func[bandit, k, sprime, 1]
            q_k = q_k0 if q_k0 > q_k1 else q_k1
            td = q_k - self.quality_func[bandit, k, s, 1] # temporal difference
            self.quality_func[bandit, k, s, 1] += learning_rate*td

            #restart action
            q_k0 = reward + self.discount_factor*self.quality_func[bandit, s, sprime, 0]
            q_k1 = reward + self.discount_factor*self.quality_func[bandit, s, sprime, 1]
            q_k = q_k0 if q_k0 > q_k1 else q_k1
            td = q_k - self.quality_func[bandit, s, k, 0] # temporal difference
            self.quality_func[bandit, s, k, 0] += learning_rate*td
        self.Gittins_Idx[bandit, s] = (1-self.discount_factor) * self.quality_func[bandit, s, s, 1]
        stop_update = timeit.default_timer()
        return stop_update - start_update # time taken to update policy

    def get_policy(self):
        """
        Returns
        -------
        pol_Gittins : ndarray
            The policy for global MDP based on the Gittins Index
        """
        # calculalte Gittins policy from Gittins Index
        if self.state_maps is None:
            self.MDP_Nb_states = self.nb_states**self.N_bandits
            self.state_maps = []
            for state in range(self.MDP_Nb_states):
                self.state_maps.append(state_to_xyz(state, self.N_bandits, self.nb_states))
        pol_Gittins = np.zeros(self.MDP_Nb_states, dtype=np.int)
        for state in range(self.MDP_Nb_states):
            xyz = self.state_maps[state]
            Gidx = np.empty(self.N_bandits)
            for b in range(self.N_bandits):
                s = xyz[b]
                Gidx[b] = (1-self.discount_factor) * self.quality_func[b, s, s, 1]
            pol_Gittins[state] = choose_arm(Gidx, self.temperature)
        return pol_Gittins
