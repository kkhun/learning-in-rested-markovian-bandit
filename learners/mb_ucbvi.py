# -*- coding: utf-8 -*-
__author__ = "Kimang Khun"
__email__ = "khun.kimang@gmail.com"

""" MB_UCBVI algorithm

Apply UCBVI algorithm

"""

from __future__ import division, print_function # Python 2 compatibility


from learners.markovian_bandit_learner import *
from utils.experiments import compute_GittinsIdx



def argmax(Gidx):
    all_ = [0]
    max_ = Gidx[0]
    for i in range(1, Gidx.shape[0]):
        if Gidx[i] > max_:
            all_ = [i]
            max_ = Gidx[i]
        elif Gidx[i] == max_:
            all_.append(i)
    return np.random.choice(all_)

class MB_UCBVI(UCRL2_Learner):
    def __init__(self, N_bandits, nb_states, discount_factor, delta, task_scheduling):
        self.delta = delta
        super().__init__(N_bandits, nb_states, discount_factor, task_scheduling)

    def reset(self):
        super().reset()
        self.Gittins_Idx = np.zeros((self.N_bandits, self.nb_states), dtype=np.float64)
        self.bonus_term = 8.0*self.nb_states*self.N_bandits / self.delta
        self.H = 1.0/(1.0 - self.discount_factor)

    def update_policy(self):
        """
        Calculate the Gittins Index for each state of each bandit:
        - For each bandit:
            - For each state:
                - Create counters for each arm
                - Compute Gittins of each arm
        """
        start_update = timeit.default_timer()   # start update
        t = max(np.sum(self.Pk), 1.0)
        hatPk = np.zeros_like(self.Pk[0])
        hatRk = np.zeros_like(self.Rk[0])
        for b in range(self.N_bandits):
            for s in range(self.nb_states):
                div = max(1.0, np.sum(self.Pk[b][s]))
                hatPk[s, :] = self.Pk[b][s, :] / div
                hatRk[s] = (self.Rk[b][s]/div) + self.H*np.sqrt(np.log(self.bonus_term *t) / (2*div))
            self.Gittins_Idx[b] = compute_GittinsIdx(hatPk, hatRk, self.discount_factor)
        stop_update = timeit.default_timer()
        #print(self.hatRk)
        return stop_update - start_update

    def choose_action(self, xyz):
        """
        Convert Global MDP state into Markvian bandit state and choose bandit according to the index

        Parameters
        ----------
        state : int
            The current state of global MDP

        Returns
        -------
        out : int
            The action for the current state
        """
        for b in range(self.N_bandits):
            if np.sum(self.Pk[b][xyz[b]]) <= 0:
                return b
        Gidx = [self.Gittins_Idx[b, xyz[b]] for b in range(self.N_bandits)]
        return argmax(np.array(Gidx))

    def name(self):
        return "MB_UCBVI"
