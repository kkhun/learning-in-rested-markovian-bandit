# -*- coding: utf-8 -*-
__author__ = "Kimang Khun"
__email__ = "khun.kimang@gmail.com"

""" Abstract class for learning algorithm

- PSRL_Learner: for PSRL style
- UCRL2_Learner: for UCRL2 style
- reset(): reset the counters of the algorithm
- update_history: update the counters given the observations

"""

from __future__ import division, print_function # Python 2 compatibility



from utils.mdp_transition_dirichlet_sampling import *
from utils.MDP_convertor import *
from utils.functions_for_ucrl2 import *
from utils.value_iteration_and_variants import *
import timeit

import utils.mdp_reward_beta_sampling as bt # bt for Beta
import utils.mdp_reward_normalGamma_sampling as gg # gg for Gaussian Gamma
post_update = [bt, gg]

class Learner(object):
    def __init__(self, N_bandits, nb_states, discount_factor, task_scheduling):
        self.N_bandits = N_bandits
        self.nb_states = nb_states
        self.discount_factor = discount_factor
        self.task_scheduling = task_scheduling
        self.state_maps = None
        self.reset()

    def name(self):
        raise Exception("name method is not implemented yet")

    def choose_action(self, state):
        raise Exception("choose_action is not implemented yet")

    def update_policy(self):
        raise Exception("update_policy is not implemented yet")

    def update_history(self, state, bandit, reward, next_state):
        """
        Parameters
        ----------
        state, next_state : int, int
            Integers between {0, nb_states**N_bandits-1} from Global MDP
            So, the conversion to Markovian bandit state is needed
        bandit : int
            The action from Global MDP
        """
        raise Exception("update_history is not implemented yet")

    def get_policy(self):
        """
        Returns
        -------
        pol_Gittins : ndarray
            The policy for global MDP based on the Gittins Index
        """
        # calculalte Gittins policy from Gittins Index
        if self.state_maps is None:
            self.MDP_Nb_states = self.nb_states**self.N_bandits
            self.state_maps = []
            for state in range(self.MDP_Nb_states):
                self.state_maps.append(state_to_xyz(state, self.N_bandits, self.nb_states))
        pol_Gittins = np.zeros(self.MDP_Nb_states, dtype=int)
        for state in range(self.MDP_Nb_states):
            xyz = self.state_maps[state]
            self.state_maps.append(xyz)
            idx = 0
            vmax = 0
            for b in range(self.N_bandits):
                if vmax < self.Gittins_Idx[b, xyz[b]]:
                    vmax = self.Gittins_Idx[b, xyz[b]]
                    idx = b
            pol_Gittins[state] = idx
        return pol_Gittins

class PSRL_Learner(Learner):
    pu_idx = 0 # pu for post_update: 0 for Beta posterior distribution
    def reset(self):
        """
        Reset the counters
        """
        self.posterior_R = []
        self.posterior_T = []
        for b in range(self.N_bandits):
            self.posterior_R.append(post_update[self.pu_idx].initial_prior_R(self.nb_states, 1))
            self.posterior_T.append(initial_prior_T(self.nb_states, 1))

    def update_history(self, xyz, bandit, reward, next_xyz):
        start_update = timeit.default_timer() # start update
        s = xyz[bandit]                 # current state of bandit
        sprime = next_xyz[bandit]       # next state of bandit
        post_update[self.pu_idx].update_posterior_R(self.posterior_R[bandit], s, 0, reward)
        update_posterior_T(self.posterior_T[bandit], s, 0, sprime)
        stop_update = timeit.default_timer() # stop update
        return stop_update - start_update

class UCRL2_Learner(Learner):
    def reset(self):
        """
        Reset the counters
        """
        self.Rk, self.Pk = [], []
        for b in range(self.N_bandits):
            self.Rk.append(np.zeros(self.nb_states, dtype=float))
            self.Pk.append(np.zeros((self.nb_states, self.nb_states), dtype=np.float64))

    def update_history(self, xyz, bandit, reward, next_xyz):
        start_update = timeit.default_timer() # start update
        s = xyz[bandit]         # current state of bandit
        sprime = next_xyz[bandit]    # next state of bandit
        self.Rk[bandit][s] += reward
        self.Pk[bandit][s, sprime] += 1
        stop_update = timeit.default_timer() # stop update
        return stop_update - start_update
