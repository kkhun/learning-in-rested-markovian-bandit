# -*- coding: utf-8 -*-
__author__ = "Kimang Khun"
__email__ = "khun.kimang@gmail.com"

""" PSRL algorithm

Algorithm from http://papers.nips.cc/paper/5185-more-efficient-reinforcement-learning-via-posterior-sampling.pdf
"""

from __future__ import division, print_function # Python 2 compatibility



from learners.markovian_bandit_learner import *

class PSRL(PSRL_Learner):
    def __init__(self, N_bandits, nb_states, discount_factor, task_scheduling, constraint=True):
        self.constraint = constraint
        self.MDP_Nb_states = int(np.power(nb_states, N_bandits))
        super().__init__(N_bandits, nb_states, discount_factor, task_scheduling)

    def reset(self):
        """
        Reset the counters
        """
        self.value_func = np.zeros(self.MDP_Nb_states, dtype=np.float64)
        self.policy = np.zeros(self.MDP_Nb_states, dtype=np.int)
        if self.constraint:
            """
            We take into account the impossible transitions in the global MDP
            """
            super().reset()
        else:
            """
            We just consider Markovian bandits as a general RL problem
            """
            self.posterior_R = post_update[self.pu_idx].initial_prior_R(self.MDP_Nb_states, self.N_bandits)
            self.posterior_T = initial_prior_T(self.MDP_Nb_states, self.N_bandits)

    def update_history(self, xyz, bandit, reward, next_xyz):
        if self.constraint:
            return super().update_history(xyz, bandit, reward, next_xyz)
        else:
            start_update = timeit.default_timer()   # start update
            state = xyz_to_state(xyz, self.N_bandits, self.nb_states)
            next_state = xyz_to_state(next_xyz, self.N_bandits, self.nb_states)
            post_update[self.pu_idx].update_posterior_R(self.posterior_R, state, bandit, reward)
            update_posterior_T(self.posterior_T, state, bandit, next_state)
            stop_update = timeit.default_timer()    # stop update
            return stop_update - start_update # time taken to do the update

    def update_policy(self):
        start_update = 0.0
        if self.constraint:
            """
            Draw MDP
            - drawing Reward vector and Transition matrix of all bandits
            - create global MDP from drawn quantities
            Do Value Iteration on global MDP that is just drawn
            """
            start_update = timeit.default_timer()   # start update
            Ps = []
            Rs = []
            for b in range(self.N_bandits):
                Ps.append(draw_transition_matrices(self.posterior_T[b])[:, 0, :])
                Rs.append(post_update[self.pu_idx].draw_reward_matrix(self.posterior_R[b])[:, 0])
            sampled_transition, sampled_reward = make_global_MDP(Ps, Rs)
        else:
            start_update = timeit.default_timer()   # start update
            sampled_transition = draw_transition_matrices(self.posterior_T)
            sampled_reward = post_update[self.pu_idx].draw_reward_matrix(self.posterior_R)
        (self.policy, self.value_func) = value_iteration(sampled_transition, sampled_reward, self.value_func, self.discount_factor)
        stop_update = timeit.default_timer()        # stop update
        return stop_update - start_update # time taken to update policy

    def choose_action(self, xyz):
        state = xyz_to_state(xyz, self.N_bandits, self.nb_states)
        return self.policy[state]

    def name(self):
        if self.constraint:
            return "PSRL withConstraint"
        else:
            return "PSRL"

    def get_policy(self):
        return self.policy
